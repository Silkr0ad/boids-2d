﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class FlockRule : MonoBehaviour {
    public abstract Vector2 Execute(Transform boid, List<Transform> neighbors);
}
