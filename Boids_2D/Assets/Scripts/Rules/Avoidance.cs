﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Avoidance : FlockRule {
    public bool on = true;
    [Header("Configuration\n")]
    public float weight = 1f;
    public float avoidanceRadius = 0.4f;
    public float curveScalar = 1f;

    public override Vector2 Execute(Transform boid, List<Transform> neighbors) {
        if (!on) {
            return Vector2.zero;
        } else { 
            Vector2 c = new Vector2();

            foreach (var neighbor in neighbors) {
                Vector2 distance = neighbor.position - boid.position;
                if (distance.magnitude < avoidanceRadius) {
                    float ratio = (avoidanceRadius - distance.magnitude) / avoidanceRadius;
                    c -= (Vector2)(neighbor.position - boid.position) * (1f / ratio * curveScalar + 1f);
                }
            }

            c *= weight;

            return c;
        }
    }
}
