﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleAvoidance : FlockRule {
    public bool on = true;
    [Header("Configuration\n")]
    public float weight = 1f;
    [Range(1f, 15f)]
    public float angleDelta = 10f;
    [Range(0.1f, 3f)]
    public float rayDistance = 1f;
    public LayerMask obstacleLayerMask;

    public override Vector2 Execute(Transform boid, List<Transform> neighbors) {
        if (!on) {
            return Vector2.zero;
        } else {
            Vector2 v2freedom = Vector2.zero;

            int i = 0;
            bool foundEscape = false;

            while (!foundEscape) {
                float angle = Mathf.Pow(-1f, i) * angleDelta * Mathf.Ceil(i / 2);
                Vector2 direction = Quaternion.Euler(0f, 0f, angle) * boid.up;

                Debug.DrawRay(boid.position, direction);

                RaycastHit2D hit = Physics2D.Raycast(boid.position, direction, rayDistance, obstacleLayerMask); // rayDistance should = boid.up.magnitude

                i++;

                if (!hit) {
                    foundEscape = true;
                    v2freedom = direction;
                } else if (Mathf.Abs(i / 2f * angleDelta) > 180f) { // if we've exhausted all possibilities... (to avoid an infinite loop)
                    break;
                }
            }

            v2freedom *= weight;

            return v2freedom;
        }
    }
}
