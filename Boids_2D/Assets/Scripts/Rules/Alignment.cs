﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Alignment : FlockRule {
    public bool on = true;
    [Header("Configuration\n")]
    public float weight = 1f;

    public override Vector2 Execute(Transform boid, List<Transform> neighbors) {
        if (!on) {
            return Vector2.zero;
        } else { 
            Vector2 pv = new Vector2();

            foreach (var neighbor in neighbors) {
                    pv += (Vector2)neighbor.transform.up;
            }

            if (neighbors.Count > 0)
                pv /= (neighbors.Count);

            pv *= weight;

            return pv;
        }
    }
}
