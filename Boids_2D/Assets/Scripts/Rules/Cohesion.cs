﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cohesion : FlockRule {
    public bool on = true;
    [Header("Configuration\n")]
    public float weight = 1f;
    private Vector2 outVelocity;
    [Range(0f, 1f)]
    public float smoothTime = .5f;

    public override Vector2 Execute(Transform boid, List<Transform> neighbors) {
        if (!on) {
            return Vector2.zero;
        } else { 
            Vector2 perceivedCenter = new Vector2();

            foreach (var neighbor in neighbors) {
                perceivedCenter += (Vector2)neighbor.transform.position;
            }

            if (neighbors.Count > 0)
                perceivedCenter /= (neighbors.Count);

            perceivedCenter -= (Vector2)boid.position;
            perceivedCenter *= weight;
            perceivedCenter = Vector2.SmoothDamp(boid.up, perceivedCenter, ref outVelocity, smoothTime);

            return perceivedCenter;
        }
    }
}
