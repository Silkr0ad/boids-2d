﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoundKeeper : MonoBehaviour {
    private void OnTriggerExit2D(Collider2D collision) {
        if (collision.tag == "Bounds") {
            transform.position = -transform.position;
        }
    }
}
