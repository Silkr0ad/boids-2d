﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(SpriteRenderer))]
public class ColorSetter : MonoBehaviour {
    public List<Color> colors;
    private SpriteRenderer sr;

    private void Awake() {
        sr = GetComponent<SpriteRenderer>();
    }

    private void Start() {
        if (colors.Count > 0) {
            int index = Random.Range(0, colors.Count - 1);
            sr.color = colors[index];
        }
    }
}
