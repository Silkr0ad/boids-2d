﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlockManager : MonoBehaviour {
    [Header("Instantiation\n")]
    public GameObject boidPrefab;
    public int boidCount;
    public Transform boidsParent;

    [Header("Rules and Configuration\n")]
    public List<FlockRule> rules;
    public List<FlockRule> postRules;
    public float maxSpeed = 5f;
    public float driveFactor = 1f;
    [Range(0f, 1f)]
    public float smoothTime = .5f;

    [Header("Neighbor Detection\n")]
    public float detectionRadius = 1.5f;
    [Range(0f, 360f)]
    public float pointOfView = 240f;
    public LayerMask boidLayerMask;

    private List<Transform> boids;

    private void Awake() {
        // initialize the boids
        boids = new List<Transform>();
        for (int i = 0; i < boidCount; i++) {
            Transform boid = Instantiate(boidPrefab, Vector2.zero, Quaternion.identity, boidsParent).transform;

            boid.position = new Vector2(Random.Range(-8f, 8f), Random.Range(-4f, 4f));
            boid.rotation = Quaternion.Euler(0f, 0f, Random.Range(-179f, 179f));
            //Vector2 randomVelocity = new Vector2(Random.Range(-1f, 1f), Random.Range(-1f, 1f)).normalized * initialSpeed;

            boids.Add(boid);
        }
    }

    private void Update() {
        foreach (var boid in boids) {
            List<Transform> neighbors = GetAllNeighbors(boid);
            Vector2 vel = new Vector2();

            foreach (var rule in rules) {
                var ret = rule.Execute(boid, neighbors);
                vel += ret;
            }

            vel = Vector2.ClampMagnitude(vel * driveFactor, maxSpeed);
            boid.position += (Vector3)vel * Time.deltaTime;
            boid.up = Vector2.Lerp(boid.up, vel, smoothTime);

            // rules that fire after all the other have finished execution (and don't need new neighbor context) go here
            vel = new Vector2();
            foreach (var rule in postRules) {
                vel += rule.Execute(boid, neighbors);
            }
            boid.position += (Vector3)vel * Time.deltaTime;
            boid.up = vel;
        }
    }

    private List<Transform> GetAllNeighbors(Transform boid) {
        List<Transform> neighbors = new List<Transform>();

        var boidC = boid.GetComponent<Collider2D>();
        var colliders = Physics2D.OverlapCircleAll(boid.position, detectionRadius, boidLayerMask);

        foreach (var c in colliders)
            if (c != boidC) {
                Vector2 delta = c.transform.position - boid.position;
                float angle = Vector2.Angle(boid.up, delta);
                if (angle * 2f < pointOfView)
                    neighbors.Add(c.transform);
            }
        return neighbors;
    }
}
