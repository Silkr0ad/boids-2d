# Boids (2D)

A simple, 2D implementation of the boids algorithm. Does not use DOTS. Yet.

![Showcase GIF](Media/boids.gif)

Most of the logic is there, but there are issues with the boids slipping though obstacles and boundaries. Could also use some tweaking in the parameters for smoother movement, maybe.

## Branches

Er... master? Didn't use Git for this project. 😅


## Attributions

A number of tutorials. Initially, I used an [old article I found online](http://www.kfish.org/boids/pseudocode.html), though [this one](https://www.red3d.com/cwr/boids/) is really good, too. Also watched some videos, the most noteworthy being [Sebastian Lague's](https://www.youtube.com/watch?v=bqtqltqcQhw) and [Dapper Dino's](https://www.youtube.com/watch?v=mjKINQigAE4&list=PL5KbKbJ6Gf99UlyIqzV1UpOzseyRn5H1d).